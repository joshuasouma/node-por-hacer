const descripcion = {

    demand: true,
    alias: 'd',
    desc: 'Descripción de tareas por hacer'

};
const completado = {

    default: true,
    alias: 'c',
    desc: 'Marcar tarea como completada'

};



const argv = require('yargs')
    .command('crear', 'Crea una tarea por hacer', {
        descripcion
    })
    .command('actualizar', 'Actualiza el estado de una tarea', {
        descripcion,
        completado
    })
    .command('listar', 'Lista todas las tareas registradas.', {})
    .command('borrar', 'Borra el elemento dado', {
        descripcion
    })
    .help()
    .argv;

module.exports = {
    argv,
}