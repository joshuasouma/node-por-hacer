//const argv = require('yargs').argv;


const { argv } = require('./config/yargs');
const colors = require('colors');
const porHacer = require('./por-hacer/por-hacer');

let comando = argv._[0];
let descripcion = argv.descripcion;
let completado = argv.completado;



switch (comando) {
    case 'crear':
        let tarea = porHacer.crear(descripcion);
        console.log(tarea);
        break;
    case 'listar':
        let listado = porHacer.getListado();
        for (let tarea of listado) {
            console.log("=============Por Hacer============".yellow);
            console.log(tarea.descripcion);
            console.log(`Estado: ${tarea.completado}`);
            console.log("==================================".yellow);

        }

        break;
    case 'actualizar':
        let actualizar = porHacer.actualizar(descripcion, completado);
        if (actualizar) {
            console.log("¡Actualizado con éxito!".green);

        } else {
            console.log("Algo falló, intente más tarde".red);

        }
        break;

    case 'borrar':
        let borrado = porHacer.borrar(descripcion);

        if (borrado) {
            console.log("¡Borrado con éxito!".green);

        } else {
            console.log("Algo falló, intente más tarde".red);

        }

        break;


    default:
        console.log('Comando no reconocido'.red);

        break;
}