const fs = require('fs');
const colors = require('colors');

let listadoPorHacer = [];

const guardarDB = async() => {
    let data = JSON.stringify(listadoPorHacer);
    fs.writeFile('./db/data.json', data, err => {
        if (err) throw new Error('Hubo un error al guardar los datos');
    });
    return '¡Datos guardados con éxito!!!';

}

const cargarDB = () => {
    try {
        listadoPorHacer = require('../db/data.json');
    } catch (error) {
        listadoPorHacer = [];
    }
}
const getListado = () => {

    try {
        listadoPorHacer = require('../db/data.json');
        return listadoPorHacer;
    } catch (error) {
        return 'No hay tareas por hacer'.blue;
    }

}




const crear = (descripcion) => {

    cargarDB();
    let porHacer = {
        descripcion,
        completado: false,
    };
    listadoPorHacer.push(porHacer);
    guardarDB()
        .then(success => console.log(success.green))
        .catch(err => console.log(err));

    return porHacer;
}

const actualizar = (descripcion, completado = true) => {
    cargarDB();
    let index = listadoPorHacer.findIndex(tarea => tarea.descripcion === descripcion);
    if (index >= 0) {
        listadoPorHacer[index].completado = completado;
        guardarDB();
        return true;
    } else {
        return false;
    }

}

const borrar = (descripcion) => {
    cargarDB();
    let index = listadoPorHacer.findIndex(pendiente => pendiente.descripcion === descripcion);
    if (index >= 0) {
        listadoPorHacer.splice(index, 1);
        guardarDB();
        return true;
    } else {
        return false;
    }
}

module.exports = {
    crear,
    getListado,
    actualizar,
    borrar
}